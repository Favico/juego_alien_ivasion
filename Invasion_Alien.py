""" Autor: Christian Condoy
    Correo: christian.condoy@unl.edu.ec
    Materia: Programación II
"""
import sys, pygame
from settings import Settings
from ship import Ship

def run_game():
    #Iniciamos el juego y creamos los objetos de Pantalla.
    pygame.init()
    ai_settings = Settings()
    screen = pygame.display.set_mode((ai_settings.screen_width,ai_settings.screen_heigth))
    screen = pygame.display.set_mode ((1200, 600))
    pygame.display.set_caption("Alien Invasiom")


    #Selecionamos el color de fondo.
    bg_color = (230,230,230)
    ship = Ship(screen)
    #Comienzo del bucle principal del Juego.
    while True:

    #Comenzamos a programar los eventos del teclado y mause.
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            screen.fill(ai_settings.bg_color)
            ship.blitme()
    #Dibuja la pantalla durante cada pasada a través del bucle.
    #Hacemos visible la pantalla mas reciente.
        pygame.display.flip()

run_game()