import pygame

class Ship():
    def __init__(self, screen):
        """Lectura y exportacion de Imagenes."""
        self.screen = screen

        # Cargue la imagen de la nave y obtenga su posición.
        self.image = pygame.image.load('resours/nave.bmp')
        self.rect = self.image.get_rect()
        self.screen_rect = screen.get_rect()
        # Comienzo de cada nave en el centro de la Pantalla .
        self.rect.centerx = self.screen_rect.centerx
        self.rect.bottom = self.screen_rect.bottom

    def blitme(self):
        """Dibuja el barco en la ubicación actual."""
        self.screen.blit(self.image, self.rect)
